<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/time/register', function () {
    return view('time.register');
});

Route::get('/time/visualize', 'TimeController@show');

Route::get('/user/register', function () {
    if(auth()->user()->isAdmin())
    {
        return view('user.register');
    }
    return view('welcome');
});

Route::get('/user/visualize', function () {
    if(auth()->user()->isAdmin())
    {
        return view('user.visualize');
    }
    return view('welcome');
});

Route::get('/user/edit/{id}', 'UsersController@edit');

Route::get('/user/edit_password/', function () {
    return view('user.edit_password');
});

Route::post('/user/update_password/', 'UsersController@updatePassword');