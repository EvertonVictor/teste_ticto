<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\User;
use DB;
class UsersController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $data = request()->validate([
                'name' => 'required',
                'email' => 'required|email',
            ]);
    
            $data = $request->all();
    
            $address = new Address();
            $address->cep = $data['cep'];
            $address->country = "Brasil";
            $address->state = $data['state'];
            $address->city = $data['city'];
            $address->district = $data['district'];
            $address->street = $data['street'];
            $address->number = $data['number'];
            $address->additional_info = $data['additional_info'];
            $address->save();
    
            $user = new User();
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->password = bcrypt($data['password']);
            $user->cpf = $data['cpf'];
            $user->birthday = $data['birthday'];
            $user->role()->associate($data['role']);
            $user->address()->associate($address);
            $user->admin_id = $data['admin_id'];
            $user->save();
    
            return Redirect::back()->with('message','Sucesso!');
        }
        catch
        (\Throwable $th)
        {
            throw $th;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try
        {
            if(auth()->user()->isAdmin())
            {
                $user = User::with('address')->find($id)->toArray();

                return view('user.edit', ["user" => $user]);
            }
            return view('welcome');
        }
        catch (\Throwable $th)
        {
            throw $th;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            $data = $request->all();

            $user = User::find($id);
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->cpf = $data['cpf'];
            $user->birthday = $data['birthday'];
            $user->role_id = $data['role_id'];
            $user->save();
    
            $address = Address::find($user->address_id);
            $address->cep = $data['cep'];
            $address->country = "Brasil";
            $address->state = $data['state'];
            $address->city = $data['city'];
            $address->district = $data['district'];
            $address->street = $data['street'];
            $address->number = $data['number'];
            $address->additional_info = $data['additional_info'];
            $address->save();
    
            return Redirect::back()->with('message','Sucesso!');
        }
        catch (\Throwable $th)
        {
            throw $th;
        }
    }

    public function updatePassword(Request $request)
    {
        try
        {
            $id = auth()->user()->id;
            $data = $request->all();

            $user = User::find($id);
            $user->password = bcrypt($data['password']);
            $user->save();
     
            return Redirect::back()->with('message','Sucesso!');
        }
        catch (\Throwable $th)
        {
            throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            User::destroy($id);

            return Redirect::back()->with('message','Sucesso!');
        }
        catch (\Throwable $th)
        {
            throw $th;
        }
    }
}
