<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Time;
use DB;

class TimeController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $data = $request->all();

            $entrance = DateTime::createFromFormat('Y-m-d H:i:s', "{$data['date']} {$data['entrance']}");
            $exit = DateTime::createFromFormat('Y-m-d H:i:s', "{$data['date']} {$data['exit']}");

            $time = new Time();
            $time->entrance = $entrance;
            $time->exit = $exit;
            $time->user_id = $data['user_id'];
            $time->save();

            return Redirect::back()->with('message','Sucesso!');
        }
        catch (\Throwable $th)
        {
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        try
        {
            if(auth()->user()->isAdmin())
            {
                $id = auth()->user()->id;

                $times = DB::select("SELECT 
                    users.id,
                    users.name,
                    roles.role,
                    TIMESTAMPDIFF(YEAR, users.birthday, CURDATE()) AS age,
                    adm.name as admin_name,
                    times.entrance,
                    times.exit
                FROM
                    users
                        INNER JOIN
                    users AS adm ON adm.id = users.admin_id
                        AND users.admin_id = {$id}
                        LEFT JOIN
                    roles ON users.role_id = roles.id
                        INNER JOIN
                    times ON times.user_id = users.id");

                return view('time.visualize', ["times"=>$times]);
            }
            return view('welcome');
        }
        catch (\Throwable $th)
        {
            throw $th;
        }
    }
}
