## teste_ticto
### Repositório do teste de desenvolvimento.

Rodar:

`composer install`

`npm install`

`npm run dev`

`php artisan migrate`

`php artisan db:seed`

`php artisan serve`


O único usuário após rodar as migrations é:
> admin@admin.com / admin

Dump do banco de dados:
> https://bitbucket.org/EvertonVictor/teste_ticto/src/master/storage/dump.zip

Em caso de dúvidas é só falar comigo!