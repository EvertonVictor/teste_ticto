<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where('role', 'Administrador')->first();
                
        $admin = new User();
        $admin->name = 'Everton';
        $admin->email = 'admin@admin.com';
        $admin->password = bcrypt('admin');
        $admin->cpf = '111.111.111-11';
        $admin->birthday = '1970-01-01';
        $admin->role()->associate($role);
        $admin->save();
    }
}
