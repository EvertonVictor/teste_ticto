<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Role();
        $admin->role = 'Administrador';
        $admin->save();

        $user = new Role();
        $user->role = 'Funcionário';
        $user->save();
    }
}