@extends("layouts.app")

@section("content")
    <div class="container">
        {!! Form::open(['url' => 'api/time']) !!}
        {!! Form::hidden('user_id', Auth::user()->id) !!}

        @php
            $times = \App\Time::where('user_id', Auth::user()->id)->get()->toArray();
        @endphp

        <div class="form-row">
            <div class="form-group col-md-2">
                    {!! Form::label("date", "Data:") !!}
                    {!! Form::date("date", null, ["class" => "form-control", "required" => "required"]) !!}
            </div>

            <div class="form-group col-md-2">
                {!! Form::label("entrance", "Hora de entrada:") !!}
                {!! Form::time('entrance', null, ["class" => "form-control", "required" => "required", "step" => "1"]) !!}
            </div>

            <div class="form-group col-md-2">
                {!! Form::label("exit", "Hora de entrada:") !!}
                {!! Form::time('exit', null, ["class" => "form-control", "required" => "required", "step" => "1"]) !!}
            </div>
        </div>

        <button type="submit" class="form-group btn btn-success"><i class="fas fa-user-clock"></i></button>        

        {!! Form::close() !!}

        <table id="date_table" class="table table-striped table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th>Data</th>
                    <th>Entrada</th>
                    <th>Saída</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($times as $time)
                <tr>
                    <td>{!! substr($time["entrance"], 0, 10) !!}</td>
                    <td>{!! $time["entrance"] !!}</td>
                    <td>{!! $time["exit"] !!}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
@endsection

@section("script")
    <script>
        $( document ).ready(function() {
            var table = $('#date_table').dataTable({searching: false, paging: true, info: false});
        });
    </script>
@endsection