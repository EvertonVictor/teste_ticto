@extends("layouts.app")

@section("content")
<div class="container">
    <div class="form-row">
        <div class="form-group col-md-6">
            {!! Form::label("begin_date", "Data inicial:") !!}
            {!! Form::date("begin_date", null, ["class" => "form-control", "required" => "required"]) !!}
        </div>

        <div class="form-group col-md-6">
            {!! Form::label("end_date", "Data final:") !!}
            {!! Form::date("end_date", null, ["class" => "form-control", "required" => "required"]) !!}
        </div>
    </div>

    <button id="search_button" type="button" class="form-group btn btn-success"><i class="fas fa-search"></i></button>

    <table id="date_table" class="table table-striped table-bordered">
        <thead class="thead-dark">
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Cargo</th>
                <th>Idade</th>
                <th>Nome Do Gestor</th>
                <th>Entrada</th>
                <th>Saída</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($times as $time)
            <tr>
                <td>{!! $time->id !!}</td>
                <td>{!! $time->name !!}</td>
                <td>{!! $time->role !!}</td>
                <td>{!! $time->age !!}</td>
                <td>{!! $time->admin_name !!}</td>
                <td>{!! $time->entrance !!}</td>
                <td>{!! $time->exit !!}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@section("script")

<script>
    $.fn.dataTableExt.afnFiltering.push(
        function(settings, searchData, index, rowData, counter) {
            let min = new Date($('#begin_date').val());
            let max = new Date($('#end_date').val());
            let date = new Date( searchData[5] ) || 0; // using the data from the 4th column

            if ( ( isNaN( min ) && isNaN( max ) ) ||
                ( isNaN( min ) && date <= max ) ||
                ( min <= date   && isNaN( max ) ) ||
                ( min <= date   && date <= max ) )
            {
                return true;
            }
            return false
            }
    );

    $( document ).ready(function() {
        var table = $('#date_table').dataTable();

        $("#search_button").on('click', function(){
            console.log("OI!");
            table.fnDraw();
        })
    });
</script>

@endsection