@extends("layouts.app")

@section("content")
    <div class="container">
        {!! Form::open(['url' => 'user/update_password/']) !!}
        {!! Form::token() !!}

        <div class="form-group col-md-6">
            {!! Form::label("password", "Senha:") !!}
            {!! Form::password("password", ["class" => "form-control", "required" => "required"]) !!}
        </div>

        <button type="submit" class="btn btn-success"><i class="fas fa-user-plus"></i></button>        

        {!! Form::close() !!}
    </div>
@endsection

@section("script")
@endsection