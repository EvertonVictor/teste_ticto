@extends("layouts.app")

@section("content")
<div class="container">
    @php
        $users = \App\User::select('id', 'name')->get()->toArray();
    @endphp

    <table id="date_table" class="table table-striped table-bordered">
        <thead class="thead-dark">
            <tr>
                <th>Nome</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr>
                <td>{!! $user["name"] !!}</td>
                <td>
                    <a href="{{ url('api/user/delete/' . $user['id'] ) }}" class="btn btn-danger" onclick="return confirm('Tem certeza?')">
                        <i class="fas fa-trash-alt"></i>
                    </a>

                    <a href="{{ url('user/edit/' . $user['id'] ) }}" class="btn btn-success">
                        <i class="fas fa-edit"></i>
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@section("script")

<script>
    $( document ).ready(function() {
        var table = $('#date_table').dataTable();
    });
</script>

@endsection