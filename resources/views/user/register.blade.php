@extends("layouts.app")

@section("content")
    <div class="container">
        {!! Form::open(['url' => 'api/user']) !!}
        {!! Form::token() !!}
        {!! Form::hidden('admin_id', Auth::user()->id) !!}

        @php
            $roles = \App\Role::pluck('role', 'id')->toArray();
        @endphp

        <div class="form-row">
            <div class="form-group col-md-6">
                {!! Form::label("name", "Nome:") !!}
                {!! Form::text("name", null, ["class" => "form-control", "required" => "required"]) !!}
            </div>

            <div class="form-group col-md-6">
                {!! Form::label("email", "E-mail:") !!}
                {!! Form::text("email", null, ["class" => "form-control", "required" => "required"]) !!}
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                {!! Form::label("cpf", "CPF:") !!}
                {!! Form::text("cpf", null, ["class" => "form-control", "required" => "required"]) !!}
            </div>

            <div class="form-group col-md-6">
                {!! Form::label("password", "Senha:") !!}
                {!! Form::password("password", ["class" => "form-control", "required" => "required"]) !!}
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                {!! Form::label("role", "Cargo:") !!}
                {!! Form::select("role", $roles, null, ["class" => "form-control", "required" => "required"]) !!}
            </div>

            <div class="form-group col-md-6">
                {!! Form::label("birthday", "Data de nascimento:") !!}
                {!! Form::date("birthday", null, ["class" => "form-control", "required" => "required"]) !!}
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-4">
                {!! Form::label("cep", "CEP:") !!}
                {!! Form::text("cep", null, ["class" => "form-control", "required" => "required", "minlength" => 9]) !!}
            </div>

            <div class="form-group col-md-4">
                {!! Form::label("state", "Estado:") !!}
                {!! Form::text("state", null, ["class" => "form-control", "readonly" => "readonly"]) !!}
            </div>

            <div class="form-group col-md-4">
                {!! Form::label("city", "Cidade:") !!}
                {!! Form::text("city", null, ["class" => "form-control", "readonly" => "readonly"]) !!}
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-4">
                {!! Form::label("district", "Bairro:") !!}
                {!! Form::text("district", null, ["class" => "form-control", "readonly" => "readonly"]) !!}
            </div>

            <div class="form-group col-md-4">
                {!! Form::label("street", "Rua:") !!}
                {!! Form::text("street", null, ["class" => "form-control", "readonly" => "readonly"]) !!}
            </div>

            <div class="form-group col-md-1">
                {!! Form::label("number", "Número:") !!}
                {!! Form::text("number", null, ["class" => "form-control", "required" => "required"]) !!}
            </div>

            <div class="form-group col-md-3">
                {!! Form::label("additional_info", "Complemento:") !!}
                {!! Form::text("additional_info", null, ["class" => "form-control"]) !!}
            </div>
        </div>

        <button type="submit" class="btn btn-success"><i class="fas fa-user-plus"></i></button>        

        {!! Form::close() !!}
    </div>
@endsection

@section("script")
    <script>
        $( document ).ready(function() {
            $("#cep").mask("00000-000");
            $("#cpf").mask("000.000.000-00");

            $("#cep").on("keyup", function(){
                let cep = $(this).val();
                if(cep.length === 9)
                {
                    $.ajax({
                        url: `https://viacep.com.br/ws/${cep}/json/`
                    }
                    )
                    .done(function(data) {
                        if(data.erro !== undefined)
                        {
                            alert("CEP inválido!");
                            $("#cep").val("");
                            $("#state").val("")
                            $("#district").val("")
                            $("#street").val("")
                        }
                        else
                        {
                            $("#state").val(data.uf)
                            $("#city").val(data.localidade)
                            $("#district").val(data.bairro)
                            $("#street").val(data.logradouro)
                        }
                        
                    })
                } 
            })

            $("#cpf").on("keyup", function(){
                let cpf = $(this).val();
                if(cpf.length === 14 && !TestaCPF(cpf.replace(/\D+/g, "")))
                {
                    alert("CPF inválido!");
                    $("#cpf").val("");
                } 
            })

            //https://www.devmedia.com.br/validar-cpf-com-javascript/23916
            //Com modificação para dígitos repetidos
            function TestaCPF(strCPF) {
                console.log(strCPF);

                var Soma;
                var Resto;
                Soma = 0;
            if (strCPF.match(/(\d)\1{10}/)) return false;
                
            for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
            Resto = (Soma * 10) % 11;
            
                if ((Resto == 10) || (Resto == 11))  Resto = 0;
                if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;
            
            Soma = 0;
                for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
                Resto = (Soma * 10) % 11;
            
                if ((Resto == 10) || (Resto == 11))  Resto = 0;
                if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
                return true;
            }

        });
    </script>
@endsection